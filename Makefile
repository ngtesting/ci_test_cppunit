######define start#########

#souce code files
SRCS = $(wildcard *.cpp)

#objects files.
OBJS = $(SRCS:.cpp = .o)

#gcc complile
CC = gcc

#g++ complie
CXX = g++

#include folders
INCLUDES = -I/

#include lib folders
LIBS = -L/usr/local/Cellar/cppunit/1.14.0/lib/libcppunit.a

#options
CCFLAGS = -g -Wall -O0 -std=c++11

######define end#########

all : $(OBJS)
	$(CXX) $(OBJS) -o main $(INCLUDES) $(LIBS) -lcppunit -ldl

%.o : %.cpp
	$(CXX) $(CCFLAGS) -c $< -o $@

clean: 
	rm -rf *.o
	rm -rf main
	rm -rf *.log *.xml

.PHONY:clean

######makefile end#########
