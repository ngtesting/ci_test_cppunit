#include "cppunit/extensions/HelperMacros.h"
//声明一个测试类
class MathTest : public CppUnit::TestFixture {
    // 添加一个TestSuite
    CPPUNIT_TEST_SUITE( MathTest );
    // 添加测试用例到TestSuite, 定义新的测试用例需要在这儿声明一下
    CPPUNIT_TEST( testAdd );
    CPPUNIT_TEST( testSub );
    // TestSuite添加完成
    CPPUNIT_TEST_SUITE_END();
protected:
    int x, y;       
public:
    MathTest() {}
    // 初始化函数
    void setUp ();
    // 清理函数
    void tearDown();
    // 测试加法的测试函数
    void testAdd ();
    // 测试减法的测试函数
    void testSub ();  
};
