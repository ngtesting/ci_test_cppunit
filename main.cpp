
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TextOutputter.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/XmlOutputter.h>

// If TestSuite is not changes, the following code don't need to change.
int main()
{
    CppUnit::TestResult r;
    CppUnit::TestResultCollector listener;
    CppUnit::TestResultCollector result;

    r.addListener(&listener);
    r.addListener(&result);

    CppUnit::TextUi::TestRunner runner;
    //Get test suit.
    CppUnit::Test *test = CppUnit::TestFactoryRegistry::getRegistry("alltest").makeTest();
    // Add this test suit to test runner.
    runner.addTest( test );

    //output log to run.log file.
    std::ofstream ofile;
    ofile.open("run.log");
    CppUnit::TextOutputter outputter(&listener, ofile);;
    //This would occur code dumped.reason unkonwn.
    //runner.setOutputter(&outputter);

    //output result to result.xml.
    std::ofstream file( "result.xml" );
    CppUnit::XmlOutputter xml( &result, file );

    //run the test runner.
    runner.run(r);

    //write to file.
    outputter.write();
    xml.write();

    //close ofstream.
    ofile.close();
    file.close();

    //output test results to console.
    CppUnit::TextOutputter o(&listener, std::cout);
    o.write();

    return listener.wasSuccessful() ? 0 : -1;
}


