CppUnitTest

Description:How to use CppUnit Test Framwork in your project.
===========

A sample test project for CppUnit with Ubuntu.

Compile : g++ -L /usr/local/lib/libcppunit.a main.cpp MathTest.cpp -lcppunit -ldl -o MathTest


Conditions:
sudo apt-get install latest version of libcppunit.
===========

Output test results to result.xml.

Output run log to run.log.

===========

Add common Makefile support.

-Complie command:
make

Excute command"
./main

-Clean command:
make clean

===========

